module gitlab.com/telecom-tower/simulator

require (
	github.com/gorilla/context v1.1.1 // indirect
	github.com/gorilla/mux v1.6.2
	github.com/gorilla/websocket v1.4.0 // indirect
	github.com/mohae/deepcopy v0.0.0-20170929034955-c48cc78d4826 // indirect
	github.com/pkg/errors v0.8.0
	github.com/sirupsen/logrus v1.1.1
	github.com/supcik/web_ws281x_go v1.0.1
	gitlab.com/telecom-tower/grpc-renderer v1.1.0
	google.golang.org/genproto v0.0.0-20181016170114-94acd270e44e // indirect
)
