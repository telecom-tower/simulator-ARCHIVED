[![GoDoc](https://godoc.org/gitlab.com/telecom-tower/simulator?status.svg)](https://godoc.org/gitlab.com/telecom-tower/simulator)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/telecom-tower/simulator)](https://goreportcard.com/report/gitlab.com/telecom-tower/simulator)
[![pipeline status](https://gitlab.com/telecom-tower/simulator/badges/master/pipeline.svg)](https://gitlab.com/telecom-tower/simulator/commits/master)
[![license](https://img.shields.io/badge/license-Apache--2.0-green.svg)](https://gitlab.com/HEIA-FR/bigdisplay)

# Telecom Tower Simulator

This is a web based simulator of the telecom tower.